﻿namespace BlackJack.DataAccess
{
    public enum _SingInResult
    {
        SingInRequest = 0,
        SingInIsDone = 1,
        SingInIsNotDone = 2,

        RegistrationRequest = 3,
        RegistrationIsDone = 4,
        RegistrationIsNotDone = 6,
    };


}
