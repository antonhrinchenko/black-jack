﻿namespace BlackJack.DataAccess
{
    public interface IUserRepository
    {
        void CloseDB();
        bool SearchUser(string username);
        string GetPassword(string username);
        void AddUser(string username, string password);
        int GetMoney(string username);
        int GetScore(string username);
        int UpdateScore(string username, int score);
        int UpdateMoney(string username, int money);
    }


}
