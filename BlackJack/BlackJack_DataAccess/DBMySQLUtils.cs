﻿using System;
using MySql.Data.MySqlClient;

using LogWriter;

namespace BlackJack.DataAccess
{
    public static class DBMySQLUtils
    {

        public static MySqlConnection GetDBConnection(string host, int port, string database, string username, string password)
        {

            string connStr = $"Server={host};Database={database};port={port};User Id={username};password={password}";

            MySqlConnection conn = new MySqlConnection(connStr);

            return conn;
        }

        public static MySqlConnection ConnectionToDatabase()
        {

            Logger.WriteLog(Const.ServerAnswer.Getting_Connection);

            MySqlConnection conn = DBUtils.GetDBConnection();

            try
            {
                Logger.WriteLog(Const.ServerAnswer.Getting_Connection);

                conn.Open();

                Logger.WriteLog(Const.ServerAnswer.Connection_Successful);
            }
            catch (Exception ex)
            {
                Logger.WriteLog($"{Const.ServerAnswer.Bad_Access} - {ex}");
            }

            return conn;
        }
    }
}
