﻿using System;
using MySql.Data.MySqlClient;
using LogWriter;

using BlackJack.Const;


namespace BlackJack.DataAccess
{

    public class UserRepository : IUserRepository
    {

        private const int basicScore = 0;
        private const int basicMoney = 100;

        private static MySqlConnection db = DBMySQLUtils.ConnectionToDatabase();
        

        public void CloseDB()
        {
            db.Close();
        }




        public bool SearchUser(string username)
        {
            var sql = $"SELECT * FROM usersBlackjack.users WHERE username='{username}';";
            MySqlCommand command = new MySqlCommand(sql, db);
            
            if (command.ExecuteScalar() != null)
            {
                Logger.WriteLog($"{Consts.UserExist} User: {username}");
                return true;
            }

            Logger.WriteLog($"User {username} not found");
            return false;
        }

        public string GetPassword(string username)
        {
            var sql = $"SELECT password FROM usersBlackjack.users WHERE username='{username}';";
            MySqlCommand command = new MySqlCommand(sql, db);
            var password = command.ExecuteScalar().ToString();
            return password;
        }


        public void AddUser(string username, string password)
        {
            MySqlConnection db = DBMySQLUtils.ConnectionToDatabase();
            var sql = $"INSERT INTO usersBlackjack.users (username, password, score, money) VALUES('{username}', '{password}', '{basicScore}', '{basicMoney}');";
            MySqlCommand command = new MySqlCommand(sql, db);
            command.ExecuteScalar();
            Logger.WriteLog($"Registration Is Done\t User: {username}");
        }

        public int GetMoney(string username)
        {
            var sql = $"SELECT money FROM usersBlackjack.users WHERE username='{username}'";
            MySqlCommand command = new MySqlCommand(sql, db);
            return Int32.Parse(command.ExecuteScalar().ToString());
        }


        public int GetScore(string username)
        {
            var sql = $"SELECT score FROM usersBlackjack.users WHERE username='{username}'";
            MySqlCommand command = new MySqlCommand(sql, db);
            return Int32.Parse(command.ExecuteScalar().ToString());
        }




        public int UpdateScore(string username, int score)
        {
            var sql = $"UPDATE `usersBlackjack`.`users` SET `score` = '{score}' WHERE(`username` = '{username}');";
            MySqlCommand command = new MySqlCommand(sql, db);
            command.ExecuteScalar();
            Logger.WriteLog($"Update score [User:{username} - Score:{score}]");
            return score;
        }

        public int UpdateMoney(string username, int money)
        {
            var sql = $"UPDATE `usersBlackjack`.`users` SET `money` = '{money}' WHERE(`username` = '{username}');";
            MySqlCommand command = new MySqlCommand(sql, db);
            command.ExecuteScalar();
            Logger.WriteLog($"Update money count [User:{username} - Money:{money}]");
            return money;
        }

        ~UserRepository()
        {
            CloseDB();
        }

    }


}
