﻿using MySql.Data.MySqlClient;

namespace BlackJack.DataAccess
{
    class DBUtils
    {
        private static string host = "localhost";
        private static int port = 3306;
        private static string database = "usersblackjack";
        private static string username = "root";
        private static string password = "270255667";


        public static MySqlConnection GetDBConnection()
        {
            return DBMySQLUtils.GetDBConnection(host, port, database, username, password);
        }
    }
}
