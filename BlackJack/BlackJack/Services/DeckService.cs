﻿using System;
using System.Collections.Generic;
using System.Text;
using BlackJack.Const;
using BlackJack.Const.Models;
using BlackJack.Services.Interfaces;

namespace BlackJack.Logic.Services
{
    public class DeckService : IDeckService
    {
        private Deck Deck { get; set; }

        public DeckService()
        {
            Deck = new Deck();
            Initialize();
        }


        public void Shuffle()
        {
            var rng = new Random();
            var cardsCount = Deck.Cards.Count;
            for (int i = cardsCount-1; i >= 0; i--)
            {
                var k = rng.Next(i + 1);
                Card card = Deck.Cards[k];
                Deck.Cards[k] = Deck.Cards[i];
                Deck.Cards[i] = card;
            }

        }

        public Card GetCard()
        {
            if (Deck.Cards.Count <= 0)
            {
                Initialize();
                Shuffle();
            }

            Card cardToReturn = Deck.Cards[Deck.Cards.Count - 1];
            Deck.Cards.RemoveAt(Deck.Cards.Count - 1);

            if (cardToReturn == null)
            {
                cardToReturn = GetCard();
            }

            return cardToReturn;
        }

        public int GetAmountOfRemainingCrads()
        {
            return Deck.Cards.Count;
        }

        public void Initialize()
        {
            Deck.Cards = new List<Card>();

            for (var i = 0; i < Consts.FaceCount; i++)
            {
                for (var j = 0; j < Consts.SuitsCount; j++)
                {
                    Deck.Cards.Add(new Card()
                    {
                        Suit = (Const.Suit)i,
                        Face = (Const.Face)j
                    });

                    if (j <= 8)
                    {
                        Deck.Cards[Deck.Cards.Count - 1].Value = j + 1;
                        continue;
                    }
                    Deck.Cards[Deck.Cards.Count - 1].Value = 10;

                }
            }
        }

    }
}

