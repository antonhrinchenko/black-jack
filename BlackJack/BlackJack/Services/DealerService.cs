﻿using BlackJack.Const.Models;
using BlackJack.Logic.Services.Interfaces;
using BlackJack.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace BlackJack.Logic.Services
{
    public class DealerService : BaseService, IDealerService
    {
        private Action<string> _drawString { get; set; }


        public DealerService(Action<string> _drawString)
            :base(_drawString)
        {
            this._drawString = _drawString;
               Hand = new List<Card>();
        }


        public int CheckCardValue(Func<Card> getCardAction)
        {
            _drawString($"[Delaer]\n");

            ShowCardsInHand();

            foreach (var card in Hand)
            {
                CardsValue += card.Value;
            }

            for (; CardsValue < 17;)
            {
                HitCard(getCardAction());
                CardsValue += Hand[Hand.Count - 1].Value;
            }

            return CardsValue;
        }

        public int ShowCardsInHandWithoutLast()
        {
            var total = 0;
            for (var i = 0; i < Hand.Count; i++)
            {
                if (i < Hand.Count - 1)
                {
                    _drawString($"Card {i + 1}: {Hand[i].Face} of {Hand[i].Suit}");
                }
                if (i == Hand.Count - 1)
                {
                    _drawString($"Card {i + 1}: [Hole Card]");
                }

                total += Hand[i].Value;
            }
            _drawString($"Total: {total}\n");
            CardsValue = total;
            return total;
        }

    }
}
