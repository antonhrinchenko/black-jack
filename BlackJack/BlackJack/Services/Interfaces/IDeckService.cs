﻿using BlackJack.Const.Models;

namespace BlackJack.Services.Interfaces
{
    public interface IDeckService
    {
        void Initialize();
        void Shuffle();
        Card GetCard();
        int GetAmountOfRemainingCrads();
        
    }
}
