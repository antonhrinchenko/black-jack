﻿using BlackJack.Const.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace BlackJack.Logic.Services.Interfaces
{
    public interface IUserService : IBaseServise
    {

        int GetBet(int UserMoney);
        bool Insurance();
        string GetUserName();
        int UpdateScore(int score);
        int UpdateMoney(int money);
        int GetScore();
        int GetMoney();
    }
}
