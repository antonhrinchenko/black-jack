﻿using BlackJack.Const.Models;
using BlackJack.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace BlackJack.Logic.Services.Interfaces
{
    public interface IBaseServise
    {
        List<Card> Hand { get; set; }
        int CardsValue { get; set; }
        int ShowCardsInHand();

        void HitCard(Card card);
        void EmptyHand();
    }
}
