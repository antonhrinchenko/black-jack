﻿using BlackJack.Const.Models;
using BlackJack.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace BlackJack.Logic.Services.Interfaces
{
    public interface IDealerService : IBaseServise
    {

        int CheckCardValue(Func<Card> getCardAction);
        int ShowCardsInHandWithoutLast();

    }
}
