﻿using BlackJack.Const.Models;
using BlackJack.Logic.Services.Interfaces;
using BlackJack.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace BlackJack.Logic.Services
{
    public class BaseService : IBaseServise
    {
        public int CardsValue { get; set; } = 0;
        public List<Card> Hand { get; set; }
        private Action<string> _drawString { get; set; }

        public BaseService(Action<string> _drawString)
        {
            this._drawString = _drawString;

        }

        public void HitCard(Card card)
        {
            Hand.Add(card);
            var count = Hand.Count;
            _drawString($"Hitted Card {count}: {Hand[count - 1].Face} of {Hand[count - 1].Suit}");
            CardsValue += Hand[Hand.Count - 1].Value;
            _drawString($"Total: {CardsValue}");
        }

        public int ShowCardsInHand()
        {
            var total = 0;
            for (var i = 0; i < Hand.Count; i++)
            {
                _drawString($"Card {i + 1}: {Hand[i].Face} of {Hand[i].Suit}");
                total += Hand[i].Value;
            }
            _drawString($"Total: {total}\n");
            CardsValue = total;
            return total;
        }

        public void EmptyHand()
        {
            for (int i = 0; i < Hand.Count; i++)
            {
                Hand.RemoveAt(i);
            }
        }


    }
}
