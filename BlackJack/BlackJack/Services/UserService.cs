﻿
using BlackJack.Const;
using BlackJack.Const.Models;
using BlackJack.DataAccess;
using BlackJack.Logic.Services.Interfaces;
using LogWriter;
using System;
using System.Collections.Generic;

namespace BlackJack.Logic.Services
{
    public class UserService : BaseService, IUserService
    {
        private IUserRepository _userRepository { get; set; }
        private string _userName { get; set; }
        public bool IsUserSingIsDone { get; set; } = false;

        private Func<string> _getUserAnswerString { get; set; }
        private Func<int> _getUserInt { get; set; }
        private Action<string> _drawString { get; set; }
        private Action _clearField { get; set; }
        public UserService(
            SingInResult command,
            Func<string>GetString,
            Func<int> _getUserInt,
            Action<string> _drawString, 
            Action _clearField 
            )
            : base(_drawString)
        {
            this._getUserInt = _getUserInt;
            this._clearField = _clearField;
            this._drawString = _drawString;
            _getUserAnswerString = GetString;

            _userRepository = new UserRepository();
             
            if(command  == SingInResult.SingInIsDone)
            {
                Hand = new List<Card>();
                IsUserSingIsDone = true;
            }
            if (command == SingInResult.SingInRequest)
            {
                IsUserSingIsDone = TrySingIn();
                if (IsUserSingIsDone)
                {
                }
            }
            if (command == SingInResult.RegistrationRequest)
            {
                IsUserSingIsDone = TryRegistrate();
                if (IsUserSingIsDone)
                {
                }
            }
            if(command == SingInResult.Exit || IsUserSingIsDone == false)
            {
                _drawString(Consts.EndGame);
                IsUserSingIsDone = false;
            }
        }


        public int GetBet(int UserMoney)
        {
            _drawString($"Money: [{UserMoney}]");

            _drawString($"How much would you like to bet? (1 - {UserMoney})");

            var bet = _getUserInt();

            if (bet < 1 || bet > UserMoney)
            {
                _drawString(Consts.IncorrectInput);
                bet = GetBet(UserMoney);
            }
            _clearField();

            return bet;
        }

        public bool Insurance()
        {
            var insurance = false;

            _drawString("Insurance? (Accept: 0 / Reject: 1)");
            var userInput = _getUserInt();

            if (userInput != 0 && userInput != 1)
            {
                _drawString(Consts.IncorrectInput);
                insurance = Insurance();
            }

            if (userInput == 0)
            {
                insurance = true;
                _drawString("\n[Insurance Accepted!]\n");

                return insurance;
            }
            if (userInput == 1)
            {
                insurance = false;

                _drawString("\n[Insurance Rejected]\n");
                return insurance;
            }

            return insurance;
        }


        private bool TryRegistrate()
        {
            return Registration();
        }
        private bool TrySingIn()
        {
            return SingIn();
        }

        public string GetUserName()
        {
            return _userName;
        }

        private bool Registration()
        {
            Logger.WriteLog(SingInResult.RegistrationRequest);
            _drawString(Consts.EnterUserName);

            var username = _getUserAnswerString();

            if (_userRepository.SearchUser(username.ToString()))
            {
                _drawString(Consts.UserExist);
                return false; 
            }

            _drawString(Consts.CreatePassword);
            string password = _getUserAnswerString();

            _drawString(Consts.EnterPassword);
            string confirmPassword = _getUserAnswerString();

            if (!password.Equals(confirmPassword))
            {
                _drawString(Consts.DifferentPasswords);
                Logger.WriteLog($"{SingInResult.RegistrationIsNotDone}\t{Consts.DifferentPasswords}");
                return false;
            }

            _clearField();
            _userRepository.AddUser(username.ToString(), password.ToString());
            _drawString($"Hello, {username}!");
            _userName = username.ToString();

            return true;
        }

        private bool SingIn()
        {
            Logger.WriteLog(SingInResult.SingInRequest);
            _drawString(Consts.EnterUserName);

            var username = _getUserAnswerString();
            if (!_userRepository.SearchUser(username.ToString()))
            {
                _drawString(Consts.UserNotFound);
                return false;
            }

            _drawString(Consts.EnterPassword);
            var password = _getUserAnswerString();
            var confirmPassword = _userRepository.GetPassword(username.ToString());

            if (!password.Equals(confirmPassword))
            {
                _drawString(Consts.IncorrectPassword);
                return false;
            }


            _clearField();
            _drawString($"Hello, {username}!");
            Logger.WriteLog($"{SingInResult.SingInIsDone}\t User: {username}");
            _userName = username.ToString();
            return true;
        }

        public int UpdateScore(int score)
        {
            return _userRepository.UpdateScore(_userName, score);
        }

        public int UpdateMoney(int money)
        {
            return _userRepository.UpdateMoney(_userName, money);
        }

        public int GetScore()
        {
            return _userRepository.GetScore(_userName);
        }

        public int GetMoney()
        {
            return _userRepository.GetMoney(_userName);
        }

    }
}
