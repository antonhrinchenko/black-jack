﻿using BlackJack.Const;
using BlackJack.Const.Enums;
using BlackJack.Const.Models;
using BlackJack.DataAccess;
using BlackJack.Logic.Services;
using BlackJack.Logic.Services.Interfaces;
using BlackJack.Services;
using BlackJack.Services.Interfaces;
using LogWriter;
using System;
using System.Collections.Generic;

namespace BlackJack
{

    public class Game
    {
        private int UserScore { get; set; } = 0;
        private int UserMoney { get; set; } = 100;
        private string _userName { get; set; }

        private readonly IDealerService _dealerService;
        private readonly IUserService _userService;
        private readonly IDeckService _deckService;


        private Func<int> _getUserInt { get; set; }
        private Func<string> _getString { get; set; }
        private Action _pressAnyKey { get; set; }
        private Action<string> _drawString { get; set; }
        private Action _clearTextField { get; set; }
        private Func<SingInResult> _singInEnum { get; set; }

        public Game(Func<string> getString, Func<int> getUserInt, Action pressAnyKey, Action<string>drawString, Action clearTextField, Func<SingInResult> singInEnum)
        {
            _deckService = new DeckService();
            this._drawString = drawString;
            this._getUserInt = getUserInt;
            this._getString = getString;
            this._pressAnyKey = pressAnyKey;
            this._clearTextField = clearTextField;
            this._singInEnum = singInEnum;


            //_userRepository = new UserRepository();


            _userService = new UserService(
              _singInEnum(),
              _getString,
              _getUserInt,
              _drawString,
              _clearTextField
              );

            _userName = _userService.GetUserName();

            _dealerService = new DealerService(_drawString);


            _drawString("Welcome\nTo BlackJack game!");
            _pressAnyKey();
            _clearTextField();

            Menu();
        }


        private void Menu()
        {
            UserMoney = GetMoney();
            UserScore = GetScore();

            _clearTextField();
            _drawString($"User:[{_userName}]" +
                $"\tMoney: [{UserMoney}]" +
                $"\tScore: [{UserScore}]");

            _drawString("\nPlay -1\nScore -2\nExit -0");


            var answer = _getUserInt(); 

            if (answer == (int)UserGameSelection.StartGame) 
            {
                Start();
            }
            if (answer == (int)UserGameSelection.ShowScore) 
            {
                _clearTextField();
                _drawString($"Score: [{UserScore}]");
                _pressAnyKey();
            } 
            if (answer == (int)UserGameSelection.QuitGame) 
            {
                _clearTextField();
                Logger.WriteLog(Consts.EndGame);
                _drawString(Consts.EndGame);
            }
            
            if (answer != 0)
            {
                Menu();
            }

        }


        private void Start()
        {
            _deckService.Shuffle();

            for (int i = 0; i < Consts.CountOfRounds; i++)
            {
                _clearTextField();


                Play();
                _drawString(Const.Consts.PressAnyKey);
                _pressAnyKey();
                _userService.UpdateMoney(UserMoney);
                _userService.UpdateScore(UserScore);

                _userService.EmptyHand();
                _dealerService.EmptyHand();

                _clearTextField();
                _drawString(Consts.GameOver);

                var stopGame = String.Empty;
                if (i < Consts.CountOfRounds-1)
                {
                    _drawString("Press X to stop game or Press C to continue");
                    stopGame = _getString();
                }
                if (i > 2 || UserMoney <= 0 || stopGame == "x")
                {
                    break;
                }
            }

        }

           
        public int UpdateScore(int score)
        {
            return _userService.UpdateScore( score);
        }

        public int UpdateMoney(int money)
        {
            return _userService.UpdateMoney( money);
        }

        public int GetScore()
        {
            return _userService.GetScore();
        }

        public int GetMoney()
        {
            return _userService.GetMoney();
        }


        private int Play()
        {
            if (_deckService.GetAmountOfRemainingCrads() < 20)
            {
                _deckService.Initialize();
                _deckService.Shuffle();
            }

            _drawString($"Remaining Cards: [{_deckService.GetAmountOfRemainingCrads()}]");

            var betAmount = _userService.GetBet(UserMoney);
            
            _userService.Hand.Add(_deckService.GetCard());
            _userService.Hand.Add(_deckService.GetCard());

            foreach (var card in _userService.Hand)
            {
                if (card.Face == Face.Ace)
                {
                    card.Value += 10;
                    break;
                }
            }

            _userService.ShowCardsInHand();

            _dealerService.Hand.Add(_deckService.GetCard());
            _dealerService.Hand.Add(_deckService.GetCard());


            foreach (var card in _dealerService.Hand)
            {
                if (card.Face == Face.Ace)
                {
                    card.Value += 10;
                    break;
                }
            }

            _drawString($"\n[Delaer]");
            _dealerService.ShowCardsInHandWithoutLast();

            var insurance = false;

            if (_dealerService.Hand[0].Face == Face.Ace)
            {
                insurance = _userService.Insurance();
            }

            if (_dealerService.Hand[0].Face == Face.Ace || _dealerService.Hand[0].Value == 10)
            {
                _drawString("Delaer checks if he has blackjack...\n");

                if (_dealerService.CardsValue == 21)
                {
                    _dealerService.ShowCardsInHand();

                    var amountLost = 0;

                    if (_userService.CardsValue == 21 && insurance)
                    {
                        amountLost = betAmount / 2;
                    }
                    if (_userService.CardsValue != 21 && !insurance)
                    {
                        amountLost = betAmount + betAmount / 2;
                    }

                    _drawString("Blackjack, Dealer wins!\n");
                    _drawString($"You lost {amountLost} chips");
                    UserMoney -= amountLost;
                    return UserMoney;
                }
                _drawString("Dealer does not have a blackjack, moving on...\n");
            }

            if (_userService.Hand[0].Value + _userService.Hand[1].Value == 21)
            {
                _drawString($"Blackjack, You Won! [{betAmount + betAmount / 2}]");
                UserMoney += betAmount + betAmount / 2;
                UserScore++;
                return UserMoney;
            }

            for( ; ;  )
            {
                var userOption = HitOrStand();
                _clearTextField();
                if (userOption.ToLower() == "h")
                {
                    _userService.HitCard(_deckService.GetCard());

                        if (_userService.CardsValue > 21)
                        {
                            _drawString($"You lost {betAmount}");
                            UserMoney -= betAmount;


                            return UserMoney;
                        }
                        if (_userService.CardsValue == 21)
                        {
                            _drawString("Good job! I assume you want to stand from now on...\n");
                            continue;
                        }
                        continue;
                }
                if (userOption.ToLower() == "s")
                {
                     return Stand(betAmount);
                }

                _pressAnyKey();
            }

        }

        string HitOrStand()
        {
            _drawString("Please choose a valid option: [(S)tand (H)it]");

            var userOption = _getString();

            if (userOption.ToLower() != "h" && userOption.ToLower() != "s")
            {
                _drawString(Consts.IncorrectInput);
                userOption = HitOrStand();
            }
            return userOption;
        }


        int Stand(int betAmount)
        {
            _dealerService.CheckCardValue(_deckService.GetCard);

            if (_dealerService.CardsValue > 21)
            {
                _drawString("Player wins!");
                _drawString($"Win money: [{betAmount}]");
                UserMoney += betAmount;
                UserScore++;
                return UserMoney;
            }

            var playerCardValue = 0;
            foreach (var card in _userService.Hand)
            {
                playerCardValue += card.Value;
            }

            if (_dealerService.CardsValue > playerCardValue)
            {
                _drawString($"\nPlayer has {playerCardValue} and dealer has {_dealerService.CardsValue}");
                _drawString("Dealer wins!");
                UserMoney -= betAmount;
                return UserMoney;
            }
            _drawString($"\nPlayer: [{playerCardValue}] \t Dealer: [{_dealerService.CardsValue}]");
            _drawString("Player wins!");
            _drawString($"Win money: [{betAmount}]");
            UserMoney += betAmount;
            UserScore++;
            return UserMoney;
        }

    }
}
