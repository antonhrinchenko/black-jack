﻿using System;

namespace BlackJack.Const
{
    public static class Consts
    {
        public const int FaceCount = 4;
        public const int SuitsCount = 13;

        public const int CountOfRounds = 3;


        public const string GameOver = "Game Over!";
        public const string IncorrectInput = "Incorrect input! Try again";
        public const string UserNotFound = "User not found!";
        public const string CorrectInput = "User input is correct";
        public const string PressAnyKey = "Press any key to continue. . . ";
        public const string EndGame = "Exit!";
        public const string SingIn = "SingIn";
        public const string Registration = "Registration";
        public const string UserExist = "User is already exist!";
        public const string CreatePassword = "Create password:";
        public const string EnterPassword = "Enter password:";
        public const string DifferentPasswords = "Passwords is different!";
        public const string IncorrectPassword = "Incorrect password!";
        public const string EnterUserName = "Enter username:";
    }
}
