﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BlackJack.Const.Enums
{
    public enum UserGameSelection
    {
        QuitGame = 0,
        StartGame = 1,
        ShowScore = 2,

    }
}
