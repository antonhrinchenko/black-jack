﻿using System;
using BlackJack.Const;
using BlackJack;

namespace BlackJack.Presentation
{
    
    public class DrawText : IDrawText
    {
        public void EnterUserNameText()
        {
            Console.WriteLine(Consts.EnterUserName);
        }

        public void CreatePassword()
        {
            Console.WriteLine(Consts.CreatePassword);
        }

        public void EnterPassword()
        {
            Console.WriteLine(Consts.EnterPassword);
        }

        public void DifferentPasswords()
        {
            Console.WriteLine(Consts.DifferentPasswords);
        }

        public void IncorrectPassword()
        {
            Console.WriteLine(Consts.IncorrectPassword);
        }

        
        public void UserExist()
        {
            Console.WriteLine(Consts.UserExist);
        }
        
        public void UserNotFound()
        {
            Console.WriteLine(Consts.UserNotFound);
        }
       
        public void HelloUser(string username)
        {
            Console.WriteLine($"Hello, {username}!");
        }
       
        public void PressAnyKey()
        {
            Console.WriteLine(Consts.PressAnyKey);
        }

       
        public void Welcome()
        {

            Console.WriteLine("Welcome\nTo BlackJack game!");
            PressAnyKey();
            Console.ReadKey();
            Console.Clear();
        }
      
        public void Menu(string User, int Money, int Score)
        {
            Console.Clear();
            Console.WriteLine($"User: [{User}] \t Money: [{Money}] \t Score: [{Score}]");
            Console.WriteLine();
            Console.WriteLine("Play -1");
            Console.WriteLine("Score -2");
            Console.WriteLine("Exit -0");
        }

        public void ShowPlayerMoney(int money)
        {
            Console.WriteLine($"Money: [{money}]");
        }

        public void ShowUserScore(int Score)
        {
            Console.Clear();
            Console.WriteLine($"Score: [{Score}]");
            PressAnyKey();
            Console.ReadKey();

        }
    
        public void Enterance()
        {
            Console.WriteLine($"{Consts.SingIn}\t-0");
            Console.WriteLine($"{Consts.Registration}\t-1");
            Console.WriteLine($"Exit\t-2");
        }
       
        public void StopGame()
        {
            Console.WriteLine("Press X to stop game or Press C to continue");
        }
       
        public void GameOver()
        {
            Console.WriteLine(Consts.GameOver);
        }
        
        
        public void IncorrectInput()
        {
            Console.WriteLine(Consts.IncorrectInput);
        }
        
        public void EndGame()
        {
            Console.WriteLine(Consts.EndGame);
        }

        
        public void LostMoney(int amountLost)
        {
            Console.WriteLine($"You lost {amountLost} chips");
        }

        public void ShowPlayerCard(string face1, string suit1, string face2, string suit2)
        {
            Console.WriteLine("[Player]");
            Console.WriteLine($"Card 1: {face1} of {suit1}");
            Console.WriteLine($"Card 2: {face2} of {suit2}");
        }

        public void ShowDelaerCard(string face1, string suit1, string face2, string suit2)
        {
            Console.WriteLine("[Delaer]");
            Console.WriteLine($"Card 1: {face1} of {suit1}");
            Console.WriteLine($"Card 2: {face2} of {suit2}");
        }

        public void ShowDelaerCard(string face1, string suit1)
        {
            Console.WriteLine("[Delaer]");
            Console.WriteLine($"Card 1: {face1} of {suit1}");
            Console.WriteLine("Card 2: [Hole Card]");
        }

        public void ShowTotalCards(int value)
        {
            Console.WriteLine($"Total: {value}");
            Console.WriteLine();
        }

        public void RemainingCards(int cards)
        {
            Console.WriteLine($"Remaining Cards: {cards}");
        }

        public void SetBet(int money)
        {
            Console.WriteLine($"How much would you like to bet? (1 - {money})");
        }

        public void IsDealerBlackJack()
        {
            Console.WriteLine("Delaer checks if he has blackjack...");
            Console.WriteLine();
        }

        public void DealerIsNotBlackJack()
        {
            Console.WriteLine("Dealer does not have a blackjack, moving on...");
            Console.WriteLine();
        }

        public void DealerBlackJackWon()
        {
            Console.WriteLine($"Blackjack, Dealer wins!\n");

        }
        public void BlackJackWon(int money)
        {
            Console.WriteLine($"Blackjack, You Won! ({money} chips)\n");

        }

        public void ShowGameResult(int playerCardValue, int dealerCardsValue)
        {
            Console.WriteLine($"Player has {playerCardValue} and dealer has {dealerCardsValue}");
        }

        public void PlayerWins()
        {
            Console.WriteLine("Player wins!");
        }

        public void DealerWins()
        {
            Console.WriteLine("Dealer wins!");
            
        }

        public void WinMoney(int win)
        {
            Console.WriteLine($"Win money: [{win}]");
        }

        public void HitOrStand()
        {
            Console.WriteLine("Please choose a valid option: [(S)tand (H)it]");
        }

        public void Hitted(string card1, string card2)
        {
            Console.WriteLine($"Hitted {card1} of {card2}");
        }

        public void Insurance()
        {
            Console.WriteLine("Insurance? (Accept: 0 / Reject: 1)");
        }

        public void InsuranceAccepted()
        {
            Console.WriteLine("\n[Insurance Accepted!]\n");
        }
        public void InsuranceRejected()
        {
            Console.WriteLine("\n[Insurance Rejected]\n");
        }
        public void HittedBlackJack()
        {
            Console.WriteLine("Good job! I assume you want to stand from now on...\n");
        }
        public void ShowCard(string value, string face, string suit)
        {
            Console.WriteLine($"Card {value}: {face} of {suit}");

        }

        public void ClearField()
        {
            Console.Clear();
        }


        public void DrawString(string text)
        {
            Console.WriteLine(text);
        }


    }
}
