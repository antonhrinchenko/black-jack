﻿namespace BlackJack.Presentation
{
    public interface IDrawText
    {
        void EnterUserNameText();
        void CreatePassword();
        void EnterPassword();
        void DifferentPasswords();
        void IncorrectPassword();
        void HelloUser(string username);
        void Menu(string User, int Money, int Score);
        void ShowUserScore(int Score);
        void UserNotFound();
        void UserExist();
        void PressAnyKey();
        void Welcome();
        void Enterance();
        void StopGame();
        void GameOver();
        void ClearField();
        void IncorrectInput();
        void EndGame();
        void LostMoney(int amountLost);
        void ShowDelaerCard(string face1, string suit1, string face2, string suit2);
        void ShowDelaerCard(string face1, string suit1);
        void ShowPlayerCard(string face1, string suit1, string face2, string suit2);
        void ShowTotalCards(int value);
        void RemainingCards(int cards);
        void ShowPlayerMoney(int money);
        void SetBet(int money);
        void IsDealerBlackJack();
        void DealerIsNotBlackJack();
        void DealerBlackJackWon();
        void BlackJackWon(int v);
        void ShowGameResult(int playerCardValue, int dealerCardsValue);
        void PlayerWins();
        void DealerWins();
        void HitOrStand();

        void Hitted(string card1, string card2);
        void Insurance();
        void InsuranceAccepted();
        void InsuranceRejected();
        void HittedBlackJack();
        void ShowCard(string value, string face, string suit);

        void DrawString(string text);
        void WinMoney(int money);
    }
}
