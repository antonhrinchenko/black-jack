﻿namespace BlackJack.Presentation
{
    public interface IGetUserAnswer
    {
        void AnyKeyPressed();
        int GetAnswerInt();
        string GetAnswerString();
    }
}
