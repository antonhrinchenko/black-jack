﻿using System;

using BlackJack.Const;
using LogWriter;


namespace BlackJack.Presentation
{

    public class GetUserAnswer : IGetUserAnswer
    {
        public  void AnyKeyPressed()
        {
            ConsoleKeyInfo userInput = Console.ReadKey(true);
        }
            

        public  int GetAnswerInt()
        {
            int answer = 9;
            try
            {
                answer = Int32.Parse(Console.ReadLine());
            }
            catch (Exception ex)
            {
                Logger.WriteLog($"{Consts.IncorrectInput} {ex}");

                Console.WriteLine(Consts.IncorrectInput);
                answer = GetAnswerInt();
            }


            return answer;
        }

        public  string GetAnswerString()
        {
            var result = String.Empty;
            try
            {
                result = Console.ReadLine();
            }
            catch (Exception ex)
            {
                Logger.WriteLog($"{Consts.IncorrectInput} {ex}");
            }

            if (result == String.Empty)
            {
                Console.WriteLine(Consts.IncorrectInput);
                result = GetAnswerString();
            }
            foreach(var item in result)
            {
                if (item == ' ')
                {
                    Console.WriteLine("Input must be without spaces");
                    result.Trim().Replace(" ", "");
                    break;
                }
            }

            return result;
        }


    }
}
