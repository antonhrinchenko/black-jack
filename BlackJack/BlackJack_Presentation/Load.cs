﻿using BlackJack.Const;
using BlackJack.Logic.Services;
using LogWriter;
using System;


namespace BlackJack.Presentation
{
    public class Load 
    {
        private string User { get; set; }
        private IDrawText DrawText { get; set; }
        private IGetUserAnswer _getUserAnswer { get; set; }

        public Load()
        {
            DrawText = new DrawText();
            _getUserAnswer = new GetUserAnswer();


            Game game = new Game(
                _getUserAnswer.GetAnswerString,
                _getUserAnswer.GetAnswerInt,
                _getUserAnswer.AnyKeyPressed,
                DrawText.DrawString,
                DrawText.ClearField,
                EnterTheMain
                );

        }

        public string GetUserName()
        {
            return User;
        }

        private SingInResult EnterTheMain()
        {
            var uiCommand = 0;
            var result = SingInResult.Exit;

            var flag = true;
            DrawText.Enterance();

            uiCommand = _getUserAnswer.GetAnswerInt();

            if (uiCommand == (int)SingInResult.SingInRequest 
                || uiCommand == (int)SingInResult.RegistrationRequest 
                || uiCommand == (int)SingInResult.Exit)
            {
                Logger.WriteLog(Consts.CorrectInput);
                DrawText.ClearField();
                flag = false;
            }


            if (uiCommand != (int)SingInResult.SingInRequest
                && uiCommand != (int)SingInResult.RegistrationRequest
                && uiCommand != (int)SingInResult.Exit)
            {
                DrawText.ClearField();
                DrawText.IncorrectInput();
            }

            if (flag)
            {
                EnterTheMain();
            }

            result = (SingInResult)uiCommand; 

            return result;
        }
    }
}
