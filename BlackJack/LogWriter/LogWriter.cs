﻿using System;
using System.IO;
using System.Text;

namespace LogWriter
{
     public static class Logger
    {
        public static void WriteLog(object msg)
        {
            try
            {
                var pathToLog = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Log");
                if (!Directory.Exists(pathToLog))
                {
                    Directory.CreateDirectory(pathToLog);
                }

                var filename = Path.Combine(pathToLog, string.Format("{0}_{1:dd.MM.yyy}.log",
                AppDomain.CurrentDomain.FriendlyName, DateTime.Now));
                var fullText = string.Format("[{0:dd.MM.yyy HH:mm:ss.fff}] [{1}]\r\n",
                DateTime.Now, msg);

                File.AppendAllText(filename, fullText, Encoding.GetEncoding("UTF-16"));

            }
            catch (Exception exeption)
            {
                Console.WriteLine($"Error! + {exeption}");
            }



        }

    }

}
